package cat712.library.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Класс-контроллер для перенаправления главных запросов
 * @version 1.0
 */
@Controller
public class MainController {
    /**
     * Получение главной страницы
     */
    @GetMapping
    public String index() {
        return "main/index";
    }
}
