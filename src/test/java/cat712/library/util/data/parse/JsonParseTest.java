package cat712.library.util.data.parse;

import cat712.library.model.Author;
import cat712.library.model.Publisher;
import cat712.library.util.data.ListAuthors;
import cat712.library.util.data.ListPublishers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class JsonParseTest {
    @Test
    @DisplayName("Десериализация списка объектов типа Author")
    public void getListAuthors() {
        Optional<ListAuthors> optionalListAuthors = JsonParse.parseAuthorsFromJSON("src/test/resources/authors.json");
        Assertions.assertTrue(optionalListAuthors.isPresent());
        ListAuthors listAuthors = optionalListAuthors.get();
        Assertions.assertNotNull(listAuthors);
        List<Author> authors = listAuthors.authors();
        Assertions.assertTrue(authors.size() > 0);
        Assertions.assertEquals(3, authors.size());
    }

    @Test
    @DisplayName("Десериализация списка объектов типа Publisher")
    public void getListPublishers() {
        Optional<ListPublishers> optionalListPublishers = JsonParse.parsePublishersFromJSON("src/test/resources/publishers.json");
        Assertions.assertTrue(optionalListPublishers.isPresent());
        ListPublishers listPublishers = optionalListPublishers.get();
        Assertions.assertNotNull(listPublishers);
        List<Publisher> publishers = listPublishers.publishers();
        Assertions.assertTrue(publishers.size() > 0);
        Assertions.assertEquals(3, publishers.size());
    }
}
