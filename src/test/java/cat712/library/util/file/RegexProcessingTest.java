package cat712.library.util.file;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class RegexProcessingTest {
    @Test
    @DisplayName("Проверка разделения пути")
    public void checkSplitPath() {
        String path = "test/test.txt";
        String[] data = RegexProcessing.getArrayAfterSplit("/", path);
        Assertions.assertEquals("test", data[0]);
        Assertions.assertEquals("test.txt", data[1]);
    }

    @Test
    @DisplayName("Получение имени файла из массива элементов")
    public void getFilenameFromArray() {
        String path = "folder/test/test.txt";
        String[] data = RegexProcessing.getArrayAfterSplit("/", path);
        String value = RegexProcessing.findFileNameFromArray(data);
        Assertions.assertFalse(value.equals(""));
        Assertions.assertEquals("test.txt", value);

        path = "folder/test/templates";
        data = RegexProcessing.getArrayAfterSplit("/", path);
        value = RegexProcessing.findFileNameFromArray(data);
        Assertions.assertTrue(value.equals(""));
        Assertions.assertEquals("", value);
    }

    @Test
    @DisplayName("Получить переименнованное имя файла")
    public void getRenameFileName() {
        String path = "test/test.txt";
        String renameFileName = RegexProcessing.renameFileCount(path, "test");
        Assertions.assertFalse(path.equals(renameFileName));
        Assertions.assertEquals("test/test2.txt", renameFileName);
    }
}
