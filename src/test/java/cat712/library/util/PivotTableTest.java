package cat712.library.util;

import cat712.library.model.*;
import cat712.library.model.component.AdditionalData;
import cat712.library.model.component.BasicData;
import cat712.library.util.file.FileProcessing;
import cat712.library.util.file.RegexProcessing;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PivotTableTest {
    private PivotTable pivotTable = new PivotTable("tests/reportTest.xslx");
    private final List<Report> reports = new ArrayList<>();

    @BeforeEach
    public void init() {
        Person person = new Person(123, new BasicData("Тестов", "Тест", "Тестович", LocalDate.of(1965, 10, 15)),
                new AdditionalData("+78112223233"));
        Book book = new Book(1, "Тестовая книга",
                new Author(10, "Антонов", "Антон", "Антонович", LocalDate.of(1922, 5, 12)),
                new Publisher(23, "Издательство"), "1980", true);

        Report report = new Report(1, person, book, LocalDateTime.now());
        try {
            Thread.sleep(3_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        report.setReturnDate(LocalDateTime.now());
        reports.add(report);
    }

    @Test
    @DisplayName("Создание отчета в виде сводной таблицы, проверка на существование и удаление")
    public void createReportAndCheckExistsAndDelete() {
        try {
            String pathToFile = pivotTable.createReport(reports);
            Assertions.assertTrue(FileProcessing.isExists(pathToFile));
            Assertions.assertTrue(FileProcessing.deleteFile(pathToFile));
            Assertions.assertFalse(FileProcessing.isExists(pathToFile));
            String directoryName = RegexProcessing.getArrayAfterSplit("/", pathToFile)[0];
            Assertions.assertTrue(FileProcessing.isExists(directoryName));
            Assertions.assertTrue(FileProcessing.deleteFile(directoryName));
            Assertions.assertFalse(FileProcessing.isExists(directoryName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
