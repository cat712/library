package cat712.library.model;

import cat712.library.model.component.AdditionalData;
import cat712.library.model.component.BasicData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class ReportTest {
    private static Report report;
    private static LocalDateTime nowOne;
    private static LocalDateTime nowTwo;

    @BeforeAll
    public static void initAll() {
        BasicData basicData = new BasicData("Тестов", "Тест", "Тестович",
                LocalDate.of(1999, 2, 20));
        AdditionalData additionalData = new AdditionalData("test@test.ru", "+71112223344");
        Person person = new Person(1, basicData, additionalData);

        Author author = new Author(1, "Иванов", "Иван", "Иванович",
                LocalDate.of(2000, 11, 10));
        Publisher publisher = new Publisher(1, "Тест");
        Book book = new Book(1, "Тест", author, publisher, "2020", true);
        nowOne = LocalDateTime.now();
        try {
            Thread.sleep(2_500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        nowTwo = LocalDateTime.now();

        report = new Report(1, person, book, nowOne, nowTwo);
    }

    @Test
    @DisplayName("Равны ли значения объекта таким же константным значениям")
    public void equalsValues() {
        Assertions.assertTrue(report.getId() == 1);
        Assertions.assertTrue(report.getPerson().equals(new Person(1, new BasicData("Тестов",
                "Тест", "Тестович", LocalDate.of(1999, 2, 20)),
                new AdditionalData("test@test.ru", "+71112223344"))));
        Assertions.assertTrue(report.getBook().equals(new Book(1, "Тест", new Author(1, "Иванов", "Иван", "Иванович",
                LocalDate.of(2000, 11, 10)), new Publisher(1, "Тест"), "2020", true)));
        Assertions.assertTrue(report.getIssueDate().equals(nowOne));
        Assertions.assertTrue(report.getReturnDate().equals(nowTwo));
    }

    @Test
    @DisplayName("Равны ли 2 объекта с одинаковыми значениями")
    public void equalsCopyObject() {
        BasicData copyBasicData = new BasicData("Тестов", "Тест", "Тестович",
                LocalDate.of(1999, 2, 20));
        AdditionalData copyAdditionalData = new AdditionalData("test@test.ru", "+71112223344");
        Person copyPerson = new Person(1, copyBasicData, copyAdditionalData);

        Author copyAuthor = new Author(1, "Иванов", "Иван", "Иванович",
                LocalDate.of(2000, 11, 10));
        Publisher copyPublisher = new Publisher(1, "Тест");
        Book copyBook = new Book(1, "Тест", copyAuthor, copyPublisher, "2020", true);

        Report copyReport = new Report(1, copyPerson, copyBook, nowOne, nowTwo);

        Assertions.assertTrue(report.equals(copyReport));
        Assertions.assertFalse(report == copyReport);
    }

    @Test
    @DisplayName("Не равны ли два объекта с разными значениями")
    public void notEqualsCopyObjectWithOtherValues() {
        BasicData anotherBasicData = new BasicData("Петров", "Михаил",
                LocalDate.of(1976, 7, 10));
        AdditionalData anotherAdditionalData = new AdditionalData("misha456@mail.ru", "+78112221565");
        Person anotherPerson = new Person(324, anotherBasicData, anotherAdditionalData);

        Author anotherAuthor = new Author(987, "Быстров", "Дмитрий", "Антонович",
                LocalDate.of(2002, 1, 5));
        Publisher anotherPublisher = new Publisher(223, "Издательство");
        Book anotherBook = new Book(45, "Книга", anotherAuthor, anotherPublisher, "2015", false);

        Report anotherReport = new Report(18, anotherPerson, anotherBook, LocalDateTime.now(), LocalDateTime.now());

        Assertions.assertFalse(report.equals(anotherReport));
    }

    @Test
    @DisplayName("Равны ли исходный и клонируемый объект")
    public void equalsCloneObject() {
        try {
            Report cloneReport = report.clone();
            Assertions.assertTrue(report.equals(cloneReport));
            Assertions.assertFalse(report == cloneReport);
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
    }
}
