package cat712.library.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

public class AuthorTest {
    private static Author author;

    @BeforeAll
    public static void initAll() {
        author = new Author(1, "Иванов", "Иван", "Иванович",
                LocalDate.of(2000, 11, 10));
    }

    @Test
    @DisplayName("Равны ли значения объекта таким же константным значениям")
    public void equalsValues() {
        Assertions.assertTrue(author.getId() == 1);
        Assertions.assertTrue(author.getLastName().equals("Иванов"));
        Assertions.assertTrue(author.getFirstName().equals("Иван"));
        Assertions.assertTrue(author.getMiddleName().equals("Иванович"));
        Assertions.assertTrue(author.getBirthDate().equals(LocalDate.of(2000, 11, 10)));
    }

    @Test
    @DisplayName("Равны ли 2 объекта с одинаковыми значениями")
    public void equalsCopyObject() {
        Author copyAuthor = new Author(1, "Иванов", "Иван", "Иванович",
                LocalDate.of(2000, 11, 10));
        Assertions.assertTrue(author.equals(copyAuthor));
        Assertions.assertFalse(author == copyAuthor);
    }

    @Test
    @DisplayName("Не равны ли два объекта с разными значениями")
    public void notEqualsCopyObjectWithOtherValues() {
        Author anotherAuthor = new Author(23, "Антонов", "Антон", "Антонович",
                LocalDate.of(1976, 5, 12));
        Assertions.assertFalse(author.equals(anotherAuthor));
    }

    @Test
    @DisplayName("Равны ли исходный и клонируемый объект")
    public void equalsCloneObject() {
        try {
            Author cloneAuthor = author.clone();
            Assertions.assertTrue(author.equals(cloneAuthor));
            Assertions.assertFalse(author == cloneAuthor);
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
    }
}
