package cat712.library.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class PublisherTest {
    private static Publisher publisher;

    @BeforeAll
    public static void initAll() {
        publisher = new Publisher(1, "Тест");
    }

    @Test
    @DisplayName("Равны ли значения объекта таким же константным значениям")
    public void equalsValues() {
        Assertions.assertTrue(publisher.getId() == 1);
        Assertions.assertTrue(publisher.getName().equals("Тест"));
    }

    @Test
    @DisplayName("Равны ли 2 объекта с одинаковыми значениями")
    public void equalsCopyObject() {
        Publisher copyPublisher = new Publisher(1, "Тест");
        Assertions.assertTrue(publisher.equals(copyPublisher));
        Assertions.assertFalse(publisher == copyPublisher);
    }

    @Test
    @DisplayName("Не равны ли два объекта с разными значениями")
    public void notEqualsCopyObjectWithOtherValues() {
        Publisher anotherPublisher = new Publisher(66, "Издательство");
        Assertions.assertFalse(publisher.equals(anotherPublisher));
    }

    @Test
    @DisplayName("Равны ли исходный и клонируемый объект")
    public void equalsCloneObject() {
        try {
            Publisher clonePublisher = publisher.clone();
            Assertions.assertTrue(publisher.equals(clonePublisher));
            Assertions.assertFalse(publisher == clonePublisher);
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
    }
}
