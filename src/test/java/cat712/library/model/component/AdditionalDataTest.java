package cat712.library.model.component;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class AdditionalDataTest {
    private static AdditionalData additionalData;

    @BeforeAll
    public static void initAll() {
        additionalData = new AdditionalData("test@test.ru", "+71112223344");
    }

    @Test
    @DisplayName("Равны ли значения объекта таким же константным значениям")
    public void equalsValues() {
        Assertions.assertTrue(additionalData.getEmail().equals("test@test.ru"));
        Assertions.assertTrue(additionalData.getPhoneNumber().equals("+71112223344"));
    }

    @Test
    @DisplayName("Равны ли 2 объекта с одинаковыми значениями")
    public void equalsCopyObject() {
        AdditionalData copyAdditionalData = new AdditionalData("test@test.ru", "+71112223344");
        Assertions.assertTrue(additionalData.equals(copyAdditionalData));
        Assertions.assertFalse(additionalData == copyAdditionalData);
    }

    @Test
    @DisplayName("Не равны ли два объекта с разными значениями")
    public void notEqualsCopyObjectWithOtherValues() {
        AdditionalData anotherAdditionalData = new AdditionalData("+71112223344");
        Assertions.assertFalse(additionalData.equals(anotherAdditionalData));

        anotherAdditionalData = new AdditionalData("anton567@mail.ru", "+79991235566");
        Assertions.assertFalse(additionalData.equals(anotherAdditionalData));

        anotherAdditionalData = new AdditionalData("+76754443212");
        Assertions.assertFalse(additionalData.equals(anotherAdditionalData));
    }

    @Test
    @DisplayName("Равны ли исходный и клонируемый объект")
    public void equalsCloneObject() {
        try {
            AdditionalData cloneAdditionalData = additionalData.clone();
            Assertions.assertTrue(additionalData.equals(cloneAdditionalData));
            Assertions.assertFalse(additionalData == cloneAdditionalData);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
