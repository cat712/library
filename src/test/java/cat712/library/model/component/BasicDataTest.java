package cat712.library.model.component;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

public class BasicDataTest {
    private static BasicData basicData;

    @BeforeAll
    public static void initAll() {
        basicData = new BasicData("Тестов", "Тест", "Тестович",
                LocalDate.of(1999, 2, 20));
    }

    @Test
    @DisplayName("Равны ли значения объекта таким же константным значениям")
    public void equalsValues() {
        Assertions.assertTrue(basicData.getLastName().equals("Тестов"));
        Assertions.assertTrue(basicData.getFirstName().equals("Тест"));
        Assertions.assertTrue(basicData.getMiddleName().equals("Тестович"));
        Assertions.assertTrue(basicData.getBirthDate().equals(LocalDate.of(1999, 2, 20)));
    }

    @Test
    @DisplayName("Равны ли 2 объекта с одинаковыми значениями")
    public void equalsCopyObject() {
        BasicData copyBasicData = new BasicData("Тестов", "Тест", "Тестович",
                LocalDate.of(1999, 2, 20));
        Assertions.assertTrue(basicData.equals(copyBasicData));
        Assertions.assertFalse(basicData == copyBasicData);
    }

    @Test
    @DisplayName("Не равны ли два объекта с разными значениями")
    public void notEqualsCopyObjectWithOtherValues() {
        BasicData anotherBasicData = new BasicData("Тестов", "Тест",
                LocalDate.of(1999, 2, 20));
        Assertions.assertFalse(basicData.equals(anotherBasicData));

        anotherBasicData = new BasicData("Иванов", "Иван", "Иванович",
                LocalDate.of(2003, 8, 21));
        Assertions.assertFalse(basicData.equals(anotherBasicData));

        anotherBasicData = new BasicData("Петров", "Андрей",
                LocalDate.of(1998, 12, 19));
        Assertions.assertFalse(basicData.equals(anotherBasicData));
    }

    @Test
    @DisplayName("Равны ли исходный и клонируемый объект")
    public void equalsCloneObject() {
        try {
            BasicData cloneBasicData = basicData.clone();
            Assertions.assertTrue(basicData.equals(cloneBasicData));
            Assertions.assertFalse(basicData == cloneBasicData);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
