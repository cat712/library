package cat712.library.model;

import cat712.library.model.component.AdditionalData;
import cat712.library.model.component.BasicData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

public class PersonTest {
    private static Person person;

    @BeforeAll
    public static void initAll() {
        BasicData basicData = new BasicData("Тестов", "Тест", "Тестович",
                LocalDate.of(1999, 2, 20));
        AdditionalData additionalData = new AdditionalData("test@test.ru", "+71112223344");
        person = new Person(1, basicData, additionalData);
    }

    @Test
    @DisplayName("Равны ли значения объекта таким же константным значениям")
    public void equalsValues() {
        Assertions.assertTrue(person.getId() == 1);
        Assertions.assertTrue(person.getBasicData().equals(new BasicData("Тестов", "Тест",
                "Тестович", LocalDate.of(1999, 2, 20))));
        Assertions.assertTrue(person.getAdditionalData().equals(new AdditionalData("test@test.ru", "+71112223344")));
    }

    @Test
    @DisplayName("Равны ли 2 объекта с одинаковыми значениями")
    public void equalsCopyObject() {
        BasicData copyBasicData = new BasicData("Тестов", "Тест", "Тестович",
                LocalDate.of(1999, 2, 20));
        AdditionalData copyAdditionalData = new AdditionalData("test@test.ru", "+71112223344");
        Person copyPerson = new Person(1, copyBasicData, copyAdditionalData);

        Assertions.assertTrue(person.equals(copyPerson));
        Assertions.assertFalse(person == copyPerson);
    }

    @Test
    @DisplayName("Не равны ли два объекта с разными значениями")
    public void notEqualsCopyObjectWithOtherValues() {
        BasicData anotherBasicData = new BasicData("Антонов", "Антон",
                LocalDate.of(1999, 2, 20));
        AdditionalData anotherAdditionalData = new AdditionalData("anton789@test.ru", "+78341234455");
        Person anotherPerson = new Person(198, anotherBasicData, anotherAdditionalData);
        Assertions.assertFalse(person.equals(anotherPerson));
    }

    @Test
    @DisplayName("Равны ли исходный и клонируемый объект")
    public void equalsCloneObject() {
        try {
            Person clonePerson = person.clone();
            Assertions.assertTrue(person.equals(clonePerson));
            Assertions.assertFalse(person == clonePerson);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
