package cat712.library.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

public class BookTest {
    private static Book book;

    @BeforeAll
    public static void initAll() {
        Author author = new Author(1, "Иванов", "Иван", "Иванович",
                LocalDate.of(2000, 11, 10));
        Publisher publisher = new Publisher(1, "Тест");
        book = new Book(1, "Тест", author, publisher, "2020", true);
    }

    @Test
    @DisplayName("Равны ли значения объекта таким же константным значениям")
    public void equalsValues() {
        Assertions.assertTrue(book.getId() == 1);
        Assertions.assertTrue(book.getName().equals("Тест"));
        Assertions.assertTrue(book.getAuthor().equals(new Author(1, "Иванов", "Иван", "Иванович",
                LocalDate.of(2000, 11, 10))));
        Assertions.assertTrue(book.getPublisher().equals(new Publisher(1, "Тест")));
        Assertions.assertTrue(book.getPublicationYear().equals("2020"));
        Assertions.assertTrue(book.isAvailable() == true);
    }

    @Test
    @DisplayName("Равны ли 2 объекта с одинаковыми значениями")
    public void equalsCopyObject() {
        Author copyAuthor = new Author(1, "Иванов", "Иван", "Иванович",
                LocalDate.of(2000, 11, 10));
        Publisher copyPublisher = new Publisher(1, "Тест");
        Book copyBook = new Book(1, "Тест", copyAuthor, copyPublisher, "2020", true);
        Assertions.assertTrue(book.equals(copyBook));
        Assertions.assertFalse(book == copyBook);
    }

    @Test
    @DisplayName("Не равны ли два объекта с разными значениями")
    public void notEqualsCopyObjectWithOtherValues() {
        Author anotherAuthor = new Author(67, "Петров", "Юрий", "Антонович",
                LocalDate.of(2000, 11, 10));
        Publisher anotherPublisher = new Publisher(1, "Тест");
        Book anotherBook = new Book(99, "Книга", anotherAuthor, anotherPublisher, "2018", false);
        Assertions.assertFalse(book.equals(anotherBook));
    }

    @Test
    @DisplayName("Равны ли исходный и клонируемый объект")
    public void equalsCloneObject() {
        try {
            Book cloneBook = book.clone();
            Assertions.assertTrue(book.equals(cloneBook));
            Assertions.assertFalse(book == cloneBook);
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
    }
}
